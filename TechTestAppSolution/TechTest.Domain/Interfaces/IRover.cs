﻿using TechTest.Domain.Entities;

namespace TechTest.Domain.Interfaces
{
    public interface IRover
    {
        int RoverPositionX { get; set; }

        int RoverPositionY { get; set; }

        Enums.RoverFacingType RoverFacing { get; set; }
    }
}
