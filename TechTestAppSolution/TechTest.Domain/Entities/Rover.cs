﻿using System;
using TechTest.Domain.Interfaces;

namespace TechTest.Domain.Entities
{
    public class Rover : IRover
    {
        public Enums.RoverFacingType RoverFacing { get; set; }

        public int RoverPositionX { get; set; }

        public int RoverPositionY { get; set; }

        public Rover(int posX, int posY, Enums.RoverFacingType facing)
        {
            RoverPositionX = posX;
            RoverPositionY = posY;
            RoverFacing = facing;
        }
    }
}
