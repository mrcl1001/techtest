﻿using System;
using System.Collections.Generic;
using TechTest.Application.Interfaces;
using TechTest.Application.Services;
using TechTest.Domain.Entities;
using TechTest.Domain.Interfaces;

namespace TechTest.Presentation
{
    public class Program
    {
        public static IApplicationBaseServices ApplicationServies;
        public static IRover RoverToManipulate;

        static void Main(string[] args)
        {
            ManageRoverPositions();
        }

        private static void ManageRoverPositions()
        {
            try
            {
                CollectionDependOnKeyTypes keyTypes = new CollectionDependOnKeyTypes();
                RoverToManipulate = new Rover(0, 0, Enums.RoverFacingType.North);

                ApplicationServies = new ApplicationBaseServices(keyTypes.GetCollectionDependOnKeyTypes(), RoverToManipulate);

                Console.WriteLine("Rover Initialization!!...");
                Console.WriteLine("PRESS L, R or F to Manipulate");

                while (true)
                {
                    Console.WriteLine(
                            ApplicationServies.RunService(Console.ReadLine())
                        );
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fail not controlled: " + ex.Message);
                Console.WriteLine("Need Restart, press Enter to exit");
                Console.ReadLine();
            }
        }
    }

    public class CollectionDependOnKeyTypes
    {
        public Dictionary<string, IUserKeyEnter_Service> GetCollectionDependOnKeyTypes()
        {
            var collectionKeyServices = new Dictionary<string, IUserKeyEnter_Service>();
            collectionKeyServices.Add("F", new F_UserKeyEnter_Service());
            collectionKeyServices.Add("L", new L_UserKeyEnter_Service());
            collectionKeyServices.Add("R", new R_UserKeyEnter_Service());

            return collectionKeyServices;
        }

    }
}
