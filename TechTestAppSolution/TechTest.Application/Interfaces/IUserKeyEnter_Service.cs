﻿using TechTest.Domain.Interfaces;

namespace TechTest.Application.Interfaces
{
    public interface IUserKeyEnter_Service
    {
        IRover RunService(IRover rover);
    }
}
