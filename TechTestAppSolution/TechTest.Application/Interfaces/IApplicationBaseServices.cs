﻿namespace TechTest.Application.Interfaces
{
    public interface IApplicationBaseServices
    {
        string RunService(string line);
    }
}
