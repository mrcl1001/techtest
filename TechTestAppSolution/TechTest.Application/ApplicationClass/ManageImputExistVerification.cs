﻿using System.Collections.Generic;
using TechTest.Application.Interfaces;

namespace TechTest.Application.ApplicationClass
{
    public class ManageImputExistVerification
    {
        public static bool GetValidKeyUserImput(string line, Dictionary<string, IUserKeyEnter_Service> RoverServices)
        {
            if (string.IsNullOrEmpty(line))
                return false;

            if (!RoverServices.ContainsKey(line))
                return false;

            return true;
        }
    }
}
