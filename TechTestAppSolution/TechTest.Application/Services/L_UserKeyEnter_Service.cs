﻿using TechTest.Application.Interfaces;
using TechTest.Domain.Entities;
using TechTest.Domain.Interfaces;

namespace TechTest.Application.Services
{
    public class L_UserKeyEnter_Service : IUserKeyEnter_Service
    {
        public L_UserKeyEnter_Service() { }

        public virtual IRover RunService(IRover rover)
        {
            rover.RoverFacing = (rover.RoverFacing == Enums.RoverFacingType.North) ? Enums.RoverFacingType.West : ((Enums.RoverFacingType)((int)rover.RoverFacing - 1));

            return rover;
        }
    }
}
