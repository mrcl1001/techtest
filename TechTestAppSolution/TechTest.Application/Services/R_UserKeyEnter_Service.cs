﻿using TechTest.Application.Interfaces;
using TechTest.Domain.Entities;
using TechTest.Domain.Interfaces;

namespace TechTest.Application.Services
{
    public class R_UserKeyEnter_Service : IUserKeyEnter_Service
    {
        public R_UserKeyEnter_Service() { }

        public virtual IRover RunService(IRover rover)
        {
            rover.RoverFacing = (rover.RoverFacing == Enums.RoverFacingType.West) ? Enums.RoverFacingType.North : ((Enums.RoverFacingType)((int)rover.RoverFacing + 1));

            return rover;
        }
    }
}
