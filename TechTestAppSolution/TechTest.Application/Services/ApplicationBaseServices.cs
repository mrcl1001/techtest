﻿using System;
using System.Collections.Generic;
using TechTest.Application.ApplicationClass;
using TechTest.Application.Interfaces;
using TechTest.Domain.Interfaces;

namespace TechTest.Application.Services
{
    public class ApplicationBaseServices : IApplicationBaseServices
    {
        #region Private Properties

        private Dictionary<string, IUserKeyEnter_Service> ManageRoverServices { get; set; }

        private IRover RoverManipulated { get; set; }

        #endregion

        #region Constructor

        public ApplicationBaseServices(Dictionary<string, IUserKeyEnter_Service> _manageRoverServices, IRover _roverManipulated)
        {
            ManageRoverServices = _manageRoverServices;

            RoverManipulated = _roverManipulated;
        }

        #endregion

        #region Public Methods

        public virtual string RunService(string line)
        {
            string line_correct = ManageImputTrimsUppet.GetValidImputEnter(line);

            if (ManageImputExistVerification.GetValidKeyUserImput(line_correct, ManageRoverServices))
            {
                RoverManipulated = ManageRoverServices[line_correct].RunService(RoverManipulated);

                return string.Format(
                        "Rover is now at {0}, {1} - facing {2}",
                        RoverManipulated.RoverPositionX,
                        RoverManipulated.RoverPositionY,
                        RoverManipulated.RoverFacing);
            }

            throw new Exception("Invalid Command");
        }

        #endregion
    }
}
