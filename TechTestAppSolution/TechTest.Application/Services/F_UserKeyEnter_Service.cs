﻿using TechTest.Application.Interfaces;
using TechTest.Domain.Entities;
using TechTest.Domain.Interfaces;

namespace TechTest.Application.Services
{
    public class F_UserKeyEnter_Service : IUserKeyEnter_Service
    {
        public F_UserKeyEnter_Service() { }

        public virtual IRover RunService(IRover rover)
        {
            if (rover.RoverFacing == Enums.RoverFacingType.North)
                rover.RoverPositionX++;
            else if (rover.RoverFacing == Enums.RoverFacingType.East)
                rover.RoverPositionY++;
            else if (rover.RoverFacing == Enums.RoverFacingType.South)
                rover.RoverPositionX--;
            else if (rover.RoverFacing == Enums.RoverFacingType.West)
                rover.RoverPositionY--;

            return rover;
        }
    }
}
