﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using TechTest.Domain.Interfaces;
using TechTest.Domain.Entities;
using TechTest.Application.Interfaces;
using TechTest.Application.Services;

namespace TechTest.UnitTest
{
    [TestClass]
    public class UnitTestRover
    {
        #region Private Properties

        private Dictionary<string, IUserKeyEnter_Service> CollectionKeyServices = null;

        public static IApplicationBaseServices ApplicationServies;

        public static IRover RoverToManipulate;

        public string LeftLetter = "L";

        public string RightLetter = "R";

        public string FrwdLetter = "F";

        #endregion

        [TestInitialize]
        public void InitializeRoverTest()
        {
            CollectionKeyServices = new Dictionary<string, IUserKeyEnter_Service>();
            CollectionKeyServices.Add(FrwdLetter, new F_UserKeyEnter_Service());
            CollectionKeyServices.Add(LeftLetter, new L_UserKeyEnter_Service());
            CollectionKeyServices.Add(RightLetter, new R_UserKeyEnter_Service());

            RoverToManipulate = new Rover(0, 0, Enums.RoverFacingType.North);

            ApplicationServies = new ApplicationBaseServices(CollectionKeyServices, RoverToManipulate);
        }

        [TestMethod]
        public void TestRover_ImputsErroneous()
        {
            try
            {
                ApplicationServies.RunService("M");
                Assert.Fail();
            }
            catch { Assert.IsNull(null); }

            try
            {
                ApplicationServies.RunService("##22");
                Assert.Fail();
            }
            catch { Assert.IsNull(null); }

            try
            {
                ApplicationServies.RunService("     ");
                Assert.Fail();
            }
            catch { Assert.IsNull(null); }

            try
            {
                ApplicationServies.RunService("M");
                Assert.Fail();
            }
            catch { Assert.IsNull(null); }
        }

        [TestMethod]
        public void TestRover_ImputsCorrect()
        {
            try { ApplicationServies.RunService(FrwdLetter); }
            catch { Assert.IsNull(null); }

            try { ApplicationServies.RunService(LeftLetter); }
            catch { Assert.IsNull(null); }

            try { ApplicationServies.RunService(RightLetter); }
            catch { Assert.IsNull(null); }
        }

        [TestMethod]
        public void TestRover_UserEnter_L()
        {
            try
            {
                RoverToManipulate.RoverFacing = Enums.RoverFacingType.North;
                string res = ApplicationServies.RunService(LeftLetter);

                Assert.AreEqual(RoverToManipulate.RoverFacing, Enums.RoverFacingType.West);
            }
            catch
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TestRover_UserEnter_R()
        {
            try
            {
                RoverToManipulate.RoverFacing = Enums.RoverFacingType.West;
                string res = ApplicationServies.RunService(RightLetter);

                Assert.AreEqual(RoverToManipulate.RoverFacing, Enums.RoverFacingType.North);
            }
            catch
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TestRover_UserEnter_F_FacingNorth()
        {
            try
            {
                RoverToManipulate.RoverFacing = Enums.RoverFacingType.North;
                int lastPosX = RoverToManipulate.RoverPositionX;
                string res = ApplicationServies.RunService(FrwdLetter);

                Assert.AreNotEqual(lastPosX, RoverToManipulate.RoverPositionX);
                Assert.AreEqual(lastPosX, RoverToManipulate.RoverPositionX - 1);
            }
            catch
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TestRover_UserEnter_F_FacingEast()
        {
            try
            {
                RoverToManipulate.RoverFacing = Enums.RoverFacingType.East;
                int lastPosY = RoverToManipulate.RoverPositionY;
                string res = ApplicationServies.RunService(FrwdLetter);

                Assert.AreNotEqual(lastPosY, RoverToManipulate.RoverPositionY);
                Assert.AreEqual(lastPosY, RoverToManipulate.RoverPositionY - 1);
            }
            catch
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TestRover_UserEnter_F_FacingSouth()
        {
            try
            {
                RoverToManipulate.RoverFacing = Enums.RoverFacingType.South;
                int lastPosX = RoverToManipulate.RoverPositionX;
                string res = ApplicationServies.RunService(FrwdLetter);

                Assert.AreNotEqual(lastPosX, RoverToManipulate.RoverPositionX);
                Assert.AreEqual(lastPosX, RoverToManipulate.RoverPositionX + 1);
            }
            catch
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TestRover_UserEnter_F_FacingWest()
        {
            try
            {
                RoverToManipulate.RoverFacing = Enums.RoverFacingType.West;
                int lastPosY = RoverToManipulate.RoverPositionY;
                string res = ApplicationServies.RunService(FrwdLetter);

                Assert.AreNotEqual(lastPosY, RoverToManipulate.RoverPositionY);
                Assert.AreEqual(lastPosY, RoverToManipulate.RoverPositionY + 1);
            }
            catch
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TestRover_Running()
        {
            try
            {
                RoverToManipulate.RoverPositionX = 0;
                RoverToManipulate.RoverPositionY = 0;
                RoverToManipulate.RoverFacing = Enums.RoverFacingType.North;

                ApplicationServies.RunService(LeftLetter);
                Assert.AreEqual(RoverToManipulate.RoverFacing, Enums.RoverFacingType.West);
                Assert.AreEqual(RoverToManipulate.RoverPositionX, 0);
                Assert.AreEqual(RoverToManipulate.RoverPositionY, 0);

                ApplicationServies.RunService(FrwdLetter);
                Assert.AreEqual(RoverToManipulate.RoverFacing, Enums.RoverFacingType.West);
                Assert.AreEqual(RoverToManipulate.RoverPositionX, 0);
                Assert.AreEqual(RoverToManipulate.RoverPositionY, -1);

                ApplicationServies.RunService(RightLetter);
                Assert.AreEqual(RoverToManipulate.RoverFacing, Enums.RoverFacingType.North);
                Assert.AreEqual(RoverToManipulate.RoverPositionX, 0);
                Assert.AreEqual(RoverToManipulate.RoverPositionY, -1);

                ApplicationServies.RunService(FrwdLetter);
                Assert.AreEqual(RoverToManipulate.RoverFacing, Enums.RoverFacingType.North);
                Assert.AreEqual(RoverToManipulate.RoverPositionX, 1);
                Assert.AreEqual(RoverToManipulate.RoverPositionY, -1);

                ApplicationServies.RunService(FrwdLetter);
                Assert.AreEqual(RoverToManipulate.RoverFacing, Enums.RoverFacingType.North);
                Assert.AreEqual(RoverToManipulate.RoverPositionX, 2);
                Assert.AreEqual(RoverToManipulate.RoverPositionY, -1);

                ApplicationServies.RunService(LeftLetter);
                Assert.AreEqual(RoverToManipulate.RoverFacing, Enums.RoverFacingType.West);
                Assert.AreEqual(RoverToManipulate.RoverPositionX, 2);
                Assert.AreEqual(RoverToManipulate.RoverPositionY, -1);

                ApplicationServies.RunService(FrwdLetter);
                Assert.AreEqual(RoverToManipulate.RoverFacing, Enums.RoverFacingType.West);
                Assert.AreEqual(RoverToManipulate.RoverPositionX, 2);
                Assert.AreEqual(RoverToManipulate.RoverPositionY, -2);
            }
            catch
            {
                Assert.Fail();
            }
        }
    }
}
